const colors = document.querySelectorAll('.color');
colors.forEach(color => {
    const back = color.lastElementChild.classList[1];
    const frt = color.lastElementChild.firstElementChild;
    console.log(frt)
    if (back) {
        const res = back.slice(8,)
        color.lastElementChild.style.backgroundColor = `${res}`
    }
    color.addEventListener('mouseenter', () => {
        color.firstElementChild.style.transform = "scale(1)";
        color.firstElementChild.style.transition = "350ms";

    })
    color.addEventListener('mouseleave', () => {
        color.firstElementChild.style.transform = "scale(0)";
        frt.innerHTML = ""
        frt.parentElement.style.opacity = "1";
        
    })
});


const colorCodes = document.querySelectorAll('.color-code');
colorCodes.forEach(colorCode => {
    colorCode.addEventListener('click', (e) => {
        const backColor = e.target.parentElement.parentElement.parentElement.lastElementChild.firstElementChild;
        const copyText = e.target.parentElement.firstElementChild;

        console.log(backColor.parentElement)
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        backColor.innerHTML = copyText.value + " Copied";
        backColor.parentElement.style.opacity = '0.7';
    })
});
