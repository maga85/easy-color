from django.apps import AppConfig


class EasycolorConfig(AppConfig):
    name = 'easycolor'
