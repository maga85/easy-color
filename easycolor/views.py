from django.db.models import Q, Count
from django.shortcuts import render, redirect,get_object_or_404, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from .forms import *
from .models import *


# for search 
def is_valid_queryparam(param):
    return param != '' and param is not None


def home(request):
    colors = Color.objects.all()
    qs = Category.objects.all().order_by('category')
    ctg = Category.objects.all().order_by('category')
    setting = Setting.objects.get(pk=1)
    category_count = qs.count()
    color_count = Color.objects.all().count()

    category = request.GET.get('category')

    if is_valid_queryparam(category) and category != 'Choose Category...':
        qs = qs.filter(category=category)

    context = {
        'colors':colors,
        'ctg':ctg,
        'qs':qs,
        'setting':setting,
        'category_count':category_count,
        'color_count':color_count,
    }
    return render(request, 'index.html', context)
def addColor(request):
    categories = Category.objects.all().order_by('category')
    title='Create'
    # form = CategoryForm(request.POST or None, request.FILES or None)
    # user = request.user
    if request.method == "POST":
        data = request.POST
        cates = request.FILES.getlist('category')
        
        if data['category'] != 'none':
            category = Category.objects.get(id=data['category'])
        elif data['category_new'] != '':
            category, created = Category.objects.get_or_create(name=data['category_new'])
        else:
            category = None

        # if data['colors'] != 'none':
        #     colors = Category.objects.get(id=data['colors'])
        # elif data['colors_new'] != '':
        #     colors, created = Category.objects.get_or_create(name=data['colors_new'])
        # else:
        #     colors = None

        for cat in cates:
            ctg = Category.objects.create(
            category=category,
            colors=colors,
            )
        return redirect('home')

    context = {
        'title':title,
        'categories':categories,

    }
    return render(request, 'add-color.html', context)

def color_delete(request, pk):
    color = get_object_or_404(Color, pk=pk)
    color.delete()
    return redirect(reverse("home"))



def error_404(request, exception):
    return render(request, '404.html', status=404)

def error_500(request):
    return render(request, '500.html', status=500)

