from django.contrib import admin
from .models import *
from django.contrib.auth.models import Group

admin.site.unregister(Group)

# class ColorInline(admin.TabularInline):
#     model = Color
#     extra = 1

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['category','update_at', 'create_at']
    list_filter = ['category']
    # inlines = [ColorInline] #Attention to this


admin.site.register(Category, CategoryAdmin)

class ColorAdmin(admin.ModelAdmin):
    list_display = ['color','back_colored','update_at', 'create_at']
    list_filter = ['color']

admin.site.register(Color, ColorAdmin)

class SettingAdmin(admin.ModelAdmin):
    list_display = ['title','description','update_at', 'create_at']
    list_filter = ['title']

admin.site.register(Setting, SettingAdmin)
