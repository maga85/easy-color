from django.urls import path
from . import views
from django.contrib.admin import helpers
from easycolor.views import error_404, error_500

urlpatterns = [
    path('', views.home, name='home'),
    path('add-color', views.addColor, name='add-color'),
    path('<int:pk>/delete/', views.color_delete, name="color-delete"),
]

handler404 = "easycolor.views.error_404"
handler500 = "easycolor.views.error_500"